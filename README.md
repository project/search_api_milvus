# Search API Milvus

## INTRODUCTION

This module provides a milvus.io backend for the
[Search API](https://www.drupal.org/project/search_api) module.

The backend uses the [milvus.io](https://www.milvus.io/) an open-source vector 
database built to power embedding similarity search and AI applications. Milvus 
makes unstructured data search more accessible, and provides a consistent user 
experience regardless of the deployment environment.

The Search API backend does not support facets or searching. It is useful just
for indexing content into the Milvus server.
The backend is capable of indexing more than one site, allowing for
federated recommendations across multiple Drupal sites.

## REQUIREMENTS

1. [Search API](https://www.drupal.org/project/search_api) module.

## INSTALLATION

The module can be installed via the
[standard Drupal installation process](https://drupal.org/node/1897420).

## CONFIGURATION

See [Search API module's README](https://www.drupal.org/node/2852816) for
instructions.

## MAINTAINERS

This module is maintained by developers at Morpht. For more information on the
company and our offerings, see [morpht.com](https://morpht.com/).
