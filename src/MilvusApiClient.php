<?php

namespace Drupal\search_api_milvus;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Psr\Http\Message\ResponseInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Milvus API client wrapper class.
 * See https://github.com/developers-savyour/laravel-milvus/blob/master/src/MilvusClient.php
 */
class MilvusApiClient {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Milvus API base url.
   *
   * @var string
   */
  protected $apiBaseUrl;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Milvus API host.
   *
   * @var string
   */
  protected $host;

  /**
   * Milvus API port.
   *
   * @var string
   */
  protected $port;

  /**
   * Milvus API version.
   *
   * @var string
   */
  protected $version;

  /**
   * Constructs a new milvusApiClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    ClientInterface $http_client,
    MessengerInterface $messenger,
    LoggerChannelFactoryInterface $logger) {
    $this->httpClient = $http_client;
    $this->messenger = $messenger;
    $this->logger = $logger->get('milvus');
  }

  /**
   * Performs a Milvus API request. Wraps the Guzzle HTTP client.
   *
   * @param string $method
   *   HTTP method.
   * @param string $endpoint
   *   The API endpoint to call.
   * @param array $body
   *   The body to send to the API endpoint.
   * @param array $query
   *   API call query parameters.
   * @param string $content_type
   *   The "Content-Type" header value.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The request response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see \GuzzleHttp\ClientInterface::request
   */
  public function request($method,
    $endpoint,
    array $body = [],
    array $query = [],
    $content_type = 'application/json') {

    $endpoint = $this->apiBaseUrl . $endpoint;

    if ($method === 'GET') {
      $endpoint = Url::fromUri($endpoint, [
        'query' => $query,
      ])->toString();
    }

    $request = [
      'headers' => [
        'Accept: application/json',
        'Content-Type' => $content_type,
      ],
      'timeout' => 10,
    ];

    if ($body) {
      $request['body'] = Json::encode($body);
    }

    try {
      $response = $this->httpClient->request($method, $endpoint, $request);

      if ($response->getStatusCode() != 200) {
        $this->messenger->addError(t('@error', [
          '@error' => $response->getReasonPhrase(),
        ]));
        $this->logger->error('Request failed: @response.', [
          '@response' => $response->getReasonPhrase(),
        ]);
      }

      $this->logger->notice('Response from %endpoint: 200 @response.', [
        '%endpoint' => $endpoint,
        '@response' => $response->getReasonPhrase(),
      ]);
    }
    catch (Exception $e) {
      $response = $e->getResponse();

      $this->logger->error('Response from %endpoint: @status_code @response.', [
        '%endpoint' => $endpoint,
        '@response' => $e->getResponse(),
        '@status_code' => $e->getResponse()->getStatusCode(),
      ]);
    }

    return $this->parseReponse($response);
  }

  /**
   * Parses the nsw_trustee API JSON response.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   API response object.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function parseReponse(ResponseInterface $response) {
    $data = $response->getBody()->getContents();
    $data = Json::decode($data);
    return $data;
  }

  /**
   * Checks the health of the Milvus API.
   *
   * @return bool
   */
  public function checkHealth() {
    $endpoint = "/health";
    try {
      return $this->request("GET", $endpoint);
    }
    catch (ClientException $e) {
      return FALSE;
    }
  }

  /**
   * Creates a collection.
   *
   * @param $collectionName
   *   The name of the collection.
   * @param $schema
   *   The schema of the collection.
   *
   * @return array
   *   The response from the API.
   */
  public function createCollection($collectionName, $schema) {
    $endpoint = "/collection";
    $body = [
      "collection_name" => $collectionName,
      "schema" => $schema,
    ];

    return $this->request("POST", $endpoint, $body);
  }

  /**
   * Loads a collection.
   *
   * @param $collectionName
   *   The name of the collection.
   *
   * @return array
   *   The response from the API.
   */
  public function loadCollection($collectionName) {
    $endpoint = "/collection/load";
    $body = [
      "collection_name" => $collectionName,
    ];

    return $this->request("POST", $endpoint, $body);
  }

  /**
   * Inserts data into a collection.
   *
   * @param $collectionName
   *   The name of the collection.
   * @param $data
   *   The data to insert.
   * @param $numRows
   *   The number of rows to insert.
   *
   * @return array
   *   The response from the API.
   */
  public function insertData($collectionName, $data, $numRows) {
    $endpoint = "/entities";
    $body = [
      "collection_name" => $collectionName,
      "fields_data" => $data,
      "num_rows" => $numRows,
    ];

    return $this->request("POST", $endpoint, $body);
  }

  /**
   * Drops a collection.
   *
   * @param $collectionName
   *   The name of the collection.
   *
   * @return array
   *   The response from the API.
   */
  public function dropCollection($collectionName) {
    $endpoint = "/collection";
    $body = [
      "collection_name" => $collectionName,
    ];

    return $this->request("DELETE", $endpoint, $body);
  }

  /**
   * Queries a collection.
   *
   * @param $collectionName
   *   The name of the collection.
   * @param $query
   *   The query to run.
   * @param $outputFields
   *   The fields to return.
   *
   * @return array
   *   The response from the API.
   */
  public function searchQuery($collectionName, $query, $outputFields) {
    $endpoint = "/query";
    $body = [
      "collection_name" => $collectionName,
      "output_fields" => $outputFields,
      "expr" => $query,
    ];
    return $this->request("POST", $endpoint, $body);
  }

  /**
   * Deletes an entity.
   *
   * @param $collectionName
   *   The name of the collection.
   * @param $query
   *   The query to delete.
   *
   * @return array
   *   The response from the API.
   */
  public function deleteEntity($collectionName, $query) {
    $endpoint = "/entities";
    $body = [
      "collection_name" => $collectionName,
      "expr" => "$query",
    ];
    return $this->request("DELETE", $endpoint, $body);
  }

  /**
   * Creates an index.
   *
   * @param $collectionName
   *   The name of the collection.
   * @param $fieldName
   *   The name of the field.
   * @param $extraParams
   *   The extra parameters.
   *
   * @return array
   *   The response from the API.
   */
  public function createIndex($collectionName, $fieldName, $extraParams) {
    $endpoint = "/index";
    $body = [
      "collection_name" => $collectionName,
      "field_name" => $fieldName,
      "extra_params" => $extraParams,
    ];

    return $this->request("POST", $endpoint, $body);
  }

  /**
   * Searches a vector.
   *
   * @param $collection
   *   The name of the collection.
   * @param $outputFields
   *   The fields to return.
   * @param $vectors
   *   The vectors to search.
   * @param $topk
   *   The number of results to return.
   * @param $annsField
   *   The field to search.
   * @param $metricType
   *   The metric type.
   *
   * @return array
   *   The response from the API.
   */
  public function searchVector($collection, $outputFields, $vectors, $topk = 100, $annsField = "embedding", $metricType = "IP") {
    $endpoint = "/search";
    $body = [
      "collection_name" => $collection,
      "output_fields" => $outputFields,
      "dsl_type" => 1,
      "search_params" => [
        ["key" => "topk", "value" => (string) $topk],
        ["key" => "anns_field", "value" => $annsField],
        ["key" => "params", "value" => json_encode(["nprobe" => 4])],
        ["key" => "metric_type", "value" => $metricType],
        ["key" => "round_decimal", "value" => "-1"],
      ],
      "vectors" => $vectors,
    ];

    return $this->request("POST", $endpoint, $body);
  }

  /**
   * Performs a hybrid search.
   *
   * @param $collection
   *   The name of the collection.
   * @param $outputFields
   *   The fields to return.
   * @param $vectors
   *   The vectors to search.
   * @param $topk
   *   The number of results to return.
   * @param $annsField
   *   The field to search.
   * @param $metricType
   *   The metric type.
   *
   * @return array
   *   The response from the API.
   */
  public function hybridSearch($collection, $outputFields, $vectors, $topk = 100, $annsField = "embedding", $metricType = "IP", $dsl) {
    $path = "{$this->version}/search";
    $body = [
      "collection_name" => $collection,
      "output_fields" => $outputFields,
      "dsl_type" => 1,
      "dsl" => $dsl,
      "search_params" => [
            ["key" => "topk", "value" => (string) $topk],
            ["key" => "anns_field", "value" => $annsField],
            ["key" => "params", "value" => json_encode(["nprobe" => 4])],
            ["key" => "metric_type", "value" => $metricType],
            ["key" => "round_decimal", "value" => "-1"],
      ],
      "vectors" => $vectors,
    ];
    return $this->sendRequest('POST', $path, $body);
  }

}
