<?php

namespace Drupal\search_api_milvus\Plugin\search_api\backend;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Serialization\Json;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;

/**
 * Indexes items using the Milvus API.
 *
 * @SearchApiBackend(
 *   id = "milvus",
 *   label = @Translation("Milvus"),
 *   description = @Translation("Indexes items using the Milvus API.")
 * )
 */
class SearchApimilvusBackend extends BackendPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * Milvus API client instance.
   *
   * @var \Probots\milvus\Client
   */
  protected $milvusClient;

  /**
   * The Milvus Index ID.
   *
   * @var string
   */
  protected $milvusIndexId;

  /**
   * The key repository.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'milvus_host' => '',
      'milvus_port' => '',
      'milvus_version' => '/api/v1',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['milvus_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#description' => $this->t('The milvus host.'),
      '#default_value' => $this->configuration['milvus_host'],
      '#required' => TRUE,
    ];

    $form['milvus_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#description' => $this->t('The milvus port.'),
      '#default_value' => $this->configuration['milvus_port'],
      '#required' => TRUE,
    ];

    $form['milvus_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Version'),
      '#description' => $this->t('The milvus API version.'),
      '#default_value' => $this->configuration['milvus_version'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    try {
      if ($milvus_client = $this->getMilvusClient()) {
        $response = $milvus_client->index()->list();
      }
    }
    catch (\Exception $e) {
      $message = $this->t('Wrong key or cluster url: @message', [
        '@message' => $e->getMessage(),
      ]);
      $form_state->setErrorByName('key', $message);
      $form_state->setErrorByName('cluster_url', $message);
      $form_state->setErrorByName('additional_headers', $message);

    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['milvus_host'] = $form_state->getValue('milvus_host');
    $this->configuration['milvus_port'] = $form_state->getValue('milvus_port');
    $this->configuration['milvus_version'] = $form_state->getValue('milvus_version');
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    $info[] = [
      'label' => $this->t('Milvus API'),
      'info' => Json::encode($this->configuration),
    ];
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function addIndex(IndexInterface $index) {
    if ($milvus_client = $this->getMilvusClient()) {
      $index_id = $this->getMilvusIndexId($index->id());
      if ($milvus_client->schema()->get()->getClasses()->isEmpty()) {
        $milvus_client->schema()->create([
          'class' => $index->id(),
          'description' => 'Created by Search API',
          'vectorizer' => 'none',
          'properties' => [
            ['dataType' => ['string'], 'name' => '_search_api_doc_source_id'],
            ['dataType' => ['string'], 'name' => '_search_api_doc_source_type'],
            ['dataType' => ['int'], 'name' => '_search_api_doc_chunk_index'],
            ['dataType' => ['text'], 'name' => '_search_api_doc_content'],
            ['dataType' => ['text'], 'name' => '_search_api_doc_metadata'],
          ],
        ]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex(IndexInterface $index) {
    if ($milvus_client = $this->getMilvusClient()) {
      $settings = $index->getThirdPartySettings('search_api_milvus');
      if (!empty($settings)) {
        $replicas = $settings['replicas'];
        $pod_type = $settings['pod_type'];

        $response = $milvus_client->index($this->getMilvusIndexId($index->id()))->configure($pod_type, $replicas);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeIndex($index) {
    if (is_object($index)) {
      try {
        if ($milvus_client = $this->getMilvusClient()) {
          $response = $milvus_client->index($this->getMilvusIndexId($index->id()))->delete();
        }
      }
      catch (\Exception $e) {
        throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    $indexed = [];

    foreach ($items as $id => $item) {
      try {
        $index = $item->getIndex();
        $vectors = [];
        $metas = [];
        $objects = [];

        foreach ($item->getFields() as $field) {
          if ($field->getType() === 'embedding') {
            foreach ($field->getValues() as $delta => $values) {
              $metas[] = $values['item'];
              $vectors[] = $values['vector'];
            }
          }

          $objects[] = [
            'id' => \Drupal::service('uuid')->generate(),
            'class' => $this->getMilvusIndexId($index_id),
            'vector' => $vectors,
            'properties' => $metas,
          ];
        }

        $this->client->batch()->create($objects);
        $indexed[] = $id;
      }
      catch (\Exception $e) {
        $this->getLogger()->error($e->getMessage());
      }
    }

    return $indexed;
  }

  /**
   * Indexes a single item on the specified index.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item to index.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   If the milvus.io API error occurs.
   */
  protected function indexItem(ItemInterface $item) {
    try {
      if ($milvus_client = $this->getMilvusClient()) {
        $index = $item->getIndex();
        $vectors = [];
        $metas = [];

        foreach ($item->getFields() as $field) {
          if ($field->getType() === 'embedding') {
            foreach ($field->getValues() as $delta => $values) {
              $metas[] = $values['item'];
              $vectors[] = $values['vector'];
            }
          }
        }

        $this->client->objects()->create([
          'id' => \Drupal::service('uuid')->generate(),
          'class' => $this->getMilvusIndexId($index_id),
          'vector' => $vectors,
          'properties' => $metas,
        ]);
      }
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    // @todo .
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    try {
      if ($milvus_client = $this->getMilvusClient()) {
        $response = $milvus->index($this->getMilvusIndexId($index->id()))->vectors()->delete($item_ids);
      }
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    try {
      if ($milvus_client = $this->getMilvusClient()) {
        $milvus_client->schema()->delete($this->getMilvusIndexId($index_id));
      }
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * Helper function to obtain the milvus API key.
   *
   * @return string
   *   The milvus API key.
   */
  protected function getMilvusApiKey() {
    if ($key = $this->configuration['key']) {
      if ($key_object = $this->keyRepository->getKey($key)) {
        return $key_object->getKeyValue();
      }
    }
  }

  /**
   * Helper function to gets the milvus client.
   *
   * @return \Probots\milvus\Client
   *   The milvus client.
   */
  protected function getMilvusClient() {
    if (!isset($this->milvusClient)) {
      try {
        if ($api_key = $this->getMilvusApiKey()) {
          $cluster_url = $this->configuration['cluster_url'];
          $additional_headers = json_decode($this->configuration['additional_headers']);
          $this->milvusClient = new milvus($cluster_url, $api_key, $additional_headers);
        }
      }
      catch (\Exception $e) {
        $message = t('There was a problem connecting to the milvus API please check your credentials: @message', [
          '@message' => $e->getMessage(),
        ]);

        throw new \Exception($message);
      }
    }

    return $this->milvusClient;
  }

  /**
   * Helper function to get the milvus Index ID.
   *
   * @param string $index_id
   *   The Search API index ID.
   *
   * @return string
   *   The milvus Index ID.
   */
  protected function getMilvusIndexId($index_id) {
    if (!$this->milvusIndexId) {
      $this->milvusIndexId = lcfirst(str_replace('_', '', ucwords($index_id, '_')));
    }

    return $this->milvusIndexId;
  }

}
